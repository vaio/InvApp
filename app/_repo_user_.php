<?php

  class repo_user
  {

   	public static function GET_USERS($conexion)
   	{

          $result_usuarios = array();

   		if (isset($conexion))
   		{
   			try
   			{
               include_once '_usuario_.php';

               $sql = "SELECT * FROM login";

               $sentencia = $conexion -> prepare($sql);

               $sentencia -> execute();

               $resultado = $sentencia -> fetchAll();

               if (count($resultado))
               {
               	   foreach ($resultado as $fila)
               	    {
               	   	   $result_usuarios[] = new usuario($fila['id'], $fila['nick'], $fila['pass']);
               	    }
               }
               else
               {
               	print "No hay resultado ";
               }

               	return $result_usuarios;

   			}
   			catch (PDOException $ex)
   			{
                print "ERROR" . $ex-> getMessage();
   			}
   		}
   	}

   	public static function GET_COUNT_USERS($conexion)
   	{
   		$TOTAL_USERS = null;

   		if (isset($conexion))
   		{
           try
           {
                $sql = "SELECT COUNT(*) as TOTAL from login";

                $sentencia = $conexion -> prepare($sql);

                $sentencia -> execute();

                $resultado = $sentencia -> fetch();

                $TOTAL_USERS = $resultado['TOTAL'];


           }
           catch (PDOException $ex)
           {
                print 'ERROR' . $ex -> getMessage();
           }
   		}

        return $TOTAL_USERS;

   	}

    public static function INSERT_USER($conexion,$usuario)
    {
        $USER_INSERT_ENABLE = false;
        
        if (isset($conexion))
         try
          {
            $sql = "insert into login (nick,pass) values (:Nick,:Password)";

            $sentencia = $conexion -> prepare($sql);

            $sentencia -> bindValue(':Nick',  $usuario -> GET_NICK(),     PDO::PARAM_STR);
            $sentencia -> bindValue(':Password', password_hash($usuario -> GET_PASSWORD(),PASSWORD_DEFAULT), PDO::PARAM_STR);

            $USER_INSERT_ENABLE = $sentencia -> execute();

          }
          catch(PDOExceptio $ex)
          {
             print 'ERROR' . $ex -> getMessage();
          }

          return $USER_INSERT_ENABLE;

    }

    public static function TRY_NAME($conexion,$nombre)
    {
        $NAME_EXIST = true;

        if(isset($conexion))
          try
             {

               $sql = "SELECT * FROM login WHERE nick= :nombre";

               $sentencia = $conexion -> prepare($sql);

               $sentencia -> bindparam(':nombre',$nombre, PDO::PARAM_STR);

               $sentencia -> execute();

               $resultado = $sentencia -> fetchAll();

               if (count($resultado))
               {
                   $NAME_EXIST = true;
               }
               else
               {
                $NAME_EXIST = false;
               }

             }
          catch (PDOException $ex)
             {
                 print 'ERROR' . $ex -> getMessage();
             }
        return $NAME_EXIST;
    }


    public static function GET_USER_BY_NICK($conexion,$nick)
    {
        $USER = null;

        if(isset($conexion))
          try
             {

               include_once '_usuario_.php';

               $sql = "SELECT * FROM login WHERE nick= :nick";

               $sentencia = $conexion -> prepare($sql);

               $sentencia -> bindparam(':nick',$nick, PDO::PARAM_STR);

               $sentencia -> execute();

               $resultado = $sentencia -> fetch();


               if (!empty($resultado))
               {
                                     $USER = new usuario(
                                         $resultado['id'],
               	   	   	                 $resultado['nick'],
               	   	   	                 $resultado['pass']);
               }

             }
          catch (PDOException $ex)
             {
                 print 'ERROR' . $ex -> getMessage();
             }
        return $USER;
    }

   }


?>
