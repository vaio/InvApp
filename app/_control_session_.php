<?php
   class control_session
   {
   	   public static function START_SESSION($id_user,$name_user)
   	   {
         if (version_compare(PHP_VERSION, '5.4.0', '<')) {
              if(session_id() == '') {session_start();}
            } else  {
              if (session_status() == PHP_SESSION_NONE) {session_start();}
          }

           $_SESSION['id'] = $id_user;
           $_SESSION['name'] = $name_user;
          
   	   }

   	   public static function CLOSE_SESSION()
   	   {

          if (session_id() == '')
           {
              session_start();
           }

           if (isset($_SESSION['id']))
           {
           	unset($_SESSION['id']);
           }


           if (isset($_SESSION['name']))
           {
           	unset($_SESSION['name']);
           }

           session_destroy();

   	   }

   	   public static function SESSION_ENABLE()
   	   {
   	   	  if (session_id() == '')
           {
              session_start();
           }

            if (isset($_SESSION['id']) && isset($_SESSION['name']))
           {
           	 return true;
           }
           else
           {
           	return false;
           }


   	   }
   }
?>
