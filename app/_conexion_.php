<?php

   class Conexion
   {

   	private static $con;

    public static function  OPEN_CONEXION()
    {
       if (!isset(self::$con))
       {
       	  try
       	  {
             include_once '_configuracion_.php';

            self::$con = new PDO('mysql:host='.SERVER_.'; dbname='.DATABASE_, USER_, PASSWORD_);
            self::$con -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            self::$con -> exec("SET CHARACTER SET utf8");

       	  }
       	  catch (PDOException $ex)
       	  {
             print "ERROR: " . $ex -> getMessage() . "<br>";
             die();
          }
       }
    }

   public static function CLOSE_CONEXION()
   {
   	 if (isset(self::$con))
   	 {

   	 	self::$con = null;

     }
   }

   public static function GET_CONEXION()
   {
   	  return self::$con;
   }


 }
 ?>
