<?php

  require '_configuracion_.php';
  require '_conexion_.php';
  #require '_redireccion_';

	$input = $_POST['input'];

  CONEXION::OPEN_CONEXION();

  $conexion = CONEXION::GET_CONEXION();

  $html= "";

  $sql = "SELECT * FROM producto  WHERE  nombre like  :input ";

  $sentencia = $conexion -> prepare($sql);

  $param = '%' . $input . '%';

  $sentencia -> bindparam(':input', $param, PDO::PARAM_STR);

  $sentencia -> execute();

  $resultado = $sentencia -> fetchAll();

  if (count($resultado))
  {
    $count = 0;

    echo 'Resultado:';

    $html.="<table class='table table-inverse' id='tabla' >
     <thead>
       <tr>
         <th>#</th>
         <th>CODIGO</th>
         <th>PRODUCTO</th>
         <th>TIPO</th>
         <th>CONTAR</th>
       </tr>
     </thead>";

      foreach ($resultado as $fila)
       {

        $count +=1;

        $tipo = $fila['tipo'];

        if($tipo == 0){
          $tipo = 'MATERIA PRIMA';
        }elseif ($tipo == 1) {
          $tipo = 'SEMI PROCESADO';
        }else {
          $tipo = 'PRODUCTO TERMINADO';
        }

        $html.= "<tr id='currItem'>
            <th>" . $count . " </th>
            <td>" . $fila['id'] ."</td>
            <td>" .$fila['nombre'] ."</td>
            <td>" . $tipo ."</td>
            <td> <a Class='btn btn-success' role='button' href='registrar?id_producto=" . $fila['id'] . "&tipo=" . $fila['tipo'] .  "&producto= ".$fila['nombre']."  '> INVENTARIAR </a></td>
          </tr>";

       }
  }else {
    $html.='<div class="alert alert-danger" role="alert">
              <strong>Ops!</strong> No se ha encontrado resultados en la busqueda.
            </div>';
  }

  CONEXION::CLOSE_CONEXION();

  $html.="<tbody>
        </tbody>
   </table>";

	echo $html;

?>
