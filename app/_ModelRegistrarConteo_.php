
<?php

require '_configuracion_.php';
require '_conexion_.php';

$bodega = $_POST['bodega'];
$id_producto = $_POST['id_producto'];
$tipo = $_POST['tipo'];
$cantidad = $_POST['cantidad'];
$usuario = $_POST['id'];

CONEXION::OPEN_CONEXION();

$conexion = CONEXION::GET_CONEXION();

$html= "";

if (isset($conexion))
     $sql = "insert into conteo (bodega,id_producto,tipo,cantidad,id_usuario) values (:bodega,:id_producto,:tipo,:cantidad,:id_usuario)";

    $sentencia = $conexion -> prepare($sql);

    $sentencia -> bindValue(':bodega',  $bodega,     PDO::PARAM_STR);
    $sentencia -> bindValue(':id_producto',  $id_producto,     PDO::PARAM_STR);
    $sentencia -> bindValue(':tipo',  $tipo,     PDO::PARAM_STR);
    $sentencia -> bindValue(':cantidad',  $cantidad,     PDO::PARAM_STR);
    $sentencia -> bindValue(':id_usuario', $usuario,     PDO::PARAM_STR);

    $INSERTED = $sentencia -> execute();

CONEXION::CLOSE_CONEXION();

if ($INSERTED){

    $html.= '<div class="alert alert-success" id="Alerta">
            <strong>Listo!</strong> Producto ha sido registrado.
          </div>';

}else {
    $html.= '<div class="alert alert-danger" id="Alerta">
            <strong>Ops!</strong> Algo marcha mal.
          </div>';
}


echo $html;
?>
