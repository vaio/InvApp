<?php

  require_once 'modelo/ModeloConteo.php';
  /**
   *
   */
  class ControladorConteo
  {

          public function NumeroItemsContados($conexion,$id_usuario){

              return ModeloConteo::GET_COUNT_CONTEO($conexion,$id_usuario);
          }

          public function ItemsContados($conexion,$id_usuario){

            return ModeloConteo::GET_CONTEO($conexion,$id_usuario);
          }
          public function DELETE_CONTEO($conexion,$id_conteo){
            ModeloConteo::DELETE_CONTEO($conexion,$id_conteo);
          }

  }



?>
