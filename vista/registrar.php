<?php

 include_once 'app/_conexion_.php';
 include_once 'app/_usuario_.php';
 include_once 'app/_control_session_.php';
 include_once 'app/_repo_user_.php';
 include_once 'app/_redireccion_.php';

 $Titulo = 'InvApp-Registrando';

 include_once 'plantilla/document_declaracion.php';

 #CONEXION::OPEN_CONEXION();

 if(!control_session::SESSION_ENABLE())
 {
   redireccion::redirigir(SERVIDOR);
 }

 $id_producto = $_GET['id_producto'];
 $producto = $_GET['producto'];
 $tipo = $_GET['tipo'];
 $id = $_SESSION['id'];

?>

<div class="container">
 <div class="message">  </div>
 <form>

   <div class="form-group row">
         <label  class="col-12 col-form-label">BODEGA:</label>
               <div class="col-12">
                  <select id="BODEGA" class="form-control">

                        <option value="2">MATERIALES</option>
                        <option value="3">PRODUCTOS EN PROCESOS</option>
                        <option value="4">PRODUCTOS TERMINADOS</option>
                        <option value="5">DEVOLUCIONES</option>
                        <option value="6">GARANTIAS</option>
                        <option value="7">RETIROS</option>

                  </select>
               </div>
   </div>
   <div class="form-group row">
         <label  class="col-12 col-form-label">TIPO PRODUCTO:</label>
               <div class="col-12">
                  <select id="TIPOPRODUCTO" class="form-control" disabled>
                        <?php
                          if ($tipo == 0){
                            echo '<option value="0">MATERIA PRIMA</option>';
                          }
                          elseif ($tipo == 1) {
                            echo '<option value="1">SEMI PROCESADOS</option>';
                          }elseif ($tipo == 2) {
                          echo '<option value="2">PRODUCTOS TERMINADOS</option>';
                        }else {
                          echo '<option value="0">MATERIA PRIMA</option>';
                          $tipo = 0;
                        }

                        ?>
                  </select>
               </div>
   </div>
   <div class="form-group row">
         <label  class="col-12 col-form-label">PRODUCTO SELECCIONADO:</label>
               <div class="col-12">
                 <input type="text" class="form-control" id="PRODUCTOSELECCIONADO"  <?php echo 'value="' . $producto . '"'; ?> disabled>
               </div>
   </div>
   <div class="form-group row">
         <label  class="col-12 col-form-label">CANTIDAD CONTADA:</label>
               <div class="col-12">
                 <input type="number" class="form-control" id="CANTIDADCONTADA" step=".01" required>
               </div>
   </div>

   <div class="form-group row">
             <div class="col-12">
               <button type="submit" class="form-control btn btn-primary" id="registrar">REGISTRAR</button>
             </div>
   </div><!-- cierre del button -->
   </form> <!-- cierre del formulario -->
<p id="Alerta"></p>
  <div class="form-group row">
    <div class="col-12">
      <a Class='form-control btn btn-success' role='button' href='<?php echo '/InvApp' ?>'> VOLVER </a>
    </div>
  </div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
   <script>

       // Attach an event for when the user submits the form
       $('form').on('submit', function(event) {

           // Prevent the page from reloading
           event.preventDefault();

           // Set the text-output span to the value of the first input
           var $bodega = $(this).find('#BODEGA');
           var bodega = $bodega.val();
           var id_producto = '<?php echo $id_producto; ?>';
           var tipo = '<?php echo $tipo; ?>';
           var $cantidad = $(this).find('#CANTIDADCONTADA');
           var cantidad = $cantidad.val();
           var id = '<?php echo $id; ?>';

           $.post("app/_ModelRegistrarConteo_.php", { bodega: bodega, id_producto : id_producto, tipo : tipo, cantidad : cantidad, id : id }, function(data){


                 $('#Alerta').replaceWith(data);
                 $('#registrar').prop("disabled",true);
   });

       });
   </script>

   <?php

     include_once 'plantilla/document_cierre.php';

   ?>
