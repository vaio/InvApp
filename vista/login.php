<?php

   include_once 'app/_configuracion_.php';
   include_once 'app/_conexion_.php';
   include_once 'app/_validar_login_.php';
   include_once 'app/_control_session_.php';
   include_once 'app/_redireccion_.php';
   include_once 'app/_repo_user_.php';

   if (control_session::SESSION_ENABLE())
   {
   	 Redireccion::redirigir(SERVIDOR);
   }

   if(isset($_POST['login']))
   {

   	 Conexion::OPEN_CONEXION();

     $validar = new validar_login($_POST['user'],$_POST['password'],Conexion::GET_CONEXION());

     if($validar -> GET_ERROR() == '' && !is_null($validar -> GET_USER()))
     {

        //Iniciar sesion
     	control_session::START_SESSION($validar -> GET_USER() -> GET_ID(), $validar -> GET_USER() -> GET_NICK());
  
      Redireccion::redirigir(SERVIDOR);

     }

     Conexion::CLOSE_CONEXION();

   }

   $Titulo ='Login';

   include_once 'plantilla/document_declaracion.php';
   //include_once 'plantilla/document_barra_navegacion.php';

?>

    <div class="container">

        <div class="message">

        </div>

         <div class="row">

         	   <div class="col-md-3">

         	   </div>

         	   <div class="col-md-6">
         	   	     <div class="panel panel-default">
         	   	     	  <div class="panel-heading text-left">
                                  <h4>Login</h4>
          	   	     	</div>
          	   	     	   <div class="panel-body">
         	   	     	            	  <form role="form" method="post" action="<?php echo RUTA_LOGIN ?>">


                                          <label for="user" class="sr-only">User</label>
         	   	     	            	  	    <input type="text" name="user" id="user" class="form-control" placeholder="User"
                                                  <?php
                                                        if (isset($_POST['login']) && isset($_POST['user']) && !empty($_POST['user']))
                                                        {
                                                        	echo 'value="' .$_POST['user'] . '"';
                                                        }
                                                  ?>
         	   	     	            	  	     required autofocus>
         	   	     	            	  	    <br>
         	   	     	            	  	    <label for="password" class="sr-only">password</label>
         	   	     	            	  	    <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
         	   	     	            	  	    <br>

         	   	     	            	  	    <?php
                                                    if(isset($_POST['login']))
                                                    {
                                                    	$validar->SHOW_ERROR();
                                                    }
         	   	     	            	  	    ?>
         	   	     	            	  	    <button type="submit" name="login" class="btn btn-lg btn-primary btn-block">
         	   	     	            	  	    	Iniciar sesion
         	   	     	            	  	    </button>
         	   	     	            	  </form>

         	   	     	            	  <br>

         	   	     	   </div>
         	   	     </div>
         	   </div>

         </div>

    </div>



 <?php

   include_once 'plantilla/document_cierre.php';

 ?>
