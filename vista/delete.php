<?php

  include_once 'app/_configuracion_.php';
  include_once 'app/_conexion_.php';
  include_once 'controlador/ControladorConteo.php';

  if(!control_session::SESSION_ENABLE())
  {
    redireccion::redirigir(SERVIDOR);
  }

  if(isset($_GET['conteo'])){

        Conexion::OPEN_CONEXION();

        $ControladorConteo = new ControladorConteo();
        echo $_GET['conteo'];
        $ControladorConteo->DELETE_CONTEO(CONEXION::GET_CONEXION(),$_GET['conteo']);

        Redireccion::redirigir(RAUTA_LISTA_CONTEO);

        Conexion::CLOSE_CONEXION();

  }

?>
