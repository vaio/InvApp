<?php

include_once 'app/_conexion_.php';
include_once 'app/_usuario_.php';
include_once 'app/_control_session_.php';
include_once 'app/_repo_user_.php';
include_once 'app/_redireccion_.php';
include_once 'controlador/ControladorConteo.php';

$Titulo = 'InvApp-Registrando';

include_once 'plantilla/document_declaracion.php';

CONEXION::OPEN_CONEXION();

if(!control_session::SESSION_ENABLE())
{
  redireccion::redirigir(SERVIDOR);
}

$ControladorConteo = new ControladorConteo();

$conteo =  $ControladorConteo->NumeroItemsContados(CONEXION::GET_CONEXION(),$_SESSION['id']);

$items = $ControladorConteo->ItemsContados(CONEXION::GET_CONEXION(),$_SESSION['id']);

CONEXION::CLOSE_CONEXION();

?>

<div class="message"></div>

<div class="container-fluid">

            <div class="form-group row">
              <div class="col-12">
                  <a class="form-control btn btn-success" href="/InvApp" role="button"> Volver </strong> </a>
                </div>
            </div>

            <table class="table table-inverse" id="tabla">
             <thead>
               <tr>
                 <th>#</th>
                 <th>BODEGA</th>
                 <th>CODIGO</th>
                 <th>PRODUCTO</th>
                 <th>TIPO</th>
                 <th>CANTIDAD</th>
                 <th>FECHA REGISTRO</th>
                 <th></th>
               </tr>
             </thead>
             <?php
                $count = 0;
                  foreach ($items as $item) {

                    $count +=1;
                    $tipo = null;
                    $bodega = null;
                    $tipo = $item->tipo;

                    if($tipo == 0){
                      $tipo = 'MATERIA PRIMA';
                    }elseif ($tipo == 1) {
                      $tipo = 'SEMI PROCESADO';
                    }else {
                      $tipo = 'PRODUCTO TERMINADO';
                    }

                    switch ($item->bodega) {

                      case '2':
                        $bodega = 'MATERIALES';
                        break;
                      case '3':
                        $bodega = 'PRODUCTOS EN PROCESOS';
                          break;
                      case '4':
                        $bodega = 'PRODUCTOS TERMINADOS';
                          break;
                      case '5':
                        $bodega = 'REPARACIONES';
                          break;
                      case '6':
                        $bodega = 'GARANTIAS';
                          break;
                      case '7':
                        $bodega = 'RETIROS';
                          break;

                      default:
                        $bodega = 'DESCONOCIDA';
                        break;
                    }

                    echo "<tr>
                          <th>" . $count . " </th>
                          <td>" . $bodega ."</td>
                          <td>" . $item->id_producto ."</td>
                          <td>" . $item->producto ."</td>
                          <td>" . $tipo ."</td>
                          <td>" . $item->cantidad ."</td>
                          <td>" . $item->fecha ."</td>
                          <td><a href='/InvApp/delete?conteo=". $item->id ."' class='btn btn-danger btn-sm'>Eliminar</a></td>
                          </tr>";
                  }

             ?>
             <tbody>

             </tbody>
           </table>

</div>
