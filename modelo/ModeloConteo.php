<?php

class ModeloConteo
{

  public static function GET_CONTEO($conexion,$id_usuario)
  {

        $CONTEO = array();

    if (isset($conexion))
    {
      try
      {

             include_once 'builder/BuilderConteo.php';

             $sql = "SELECT c.id,
                          	     bodega,
                                 id_producto,
                                 p.nombre,
                                 c.tipo,
                                 cantidad,
                                 id_usuario,
                                 nick,
                                 fecha
                                 FROM conteo c inner join login l on c.id_usuario = l.id
                                 inner join producto p on c.id_producto = p.id
                                 WHERE id_usuario=:usuario order by bodega";

             $sentencia = $conexion -> prepare($sql);

             $sentencia -> bindValue(':usuario',  $id_usuario, PDO::PARAM_STR);

             $sentencia -> execute();

             $resultado = $sentencia -> fetchAll();

             if (count($resultado))
             {
                 foreach ($resultado as $fila)
                  {
                     $Conteo = new BuilderConteo();

                     $Conteo->id =$fila['id'];
                     $Conteo->bodega = $fila['bodega'];
                     $Conteo->id_producto =$fila['id_producto'];
                     $Conteo->producto =$fila['nombre'];;
                     $Conteo->tipo =$fila['tipo'];
                     $Conteo->cantidad=$fila['cantidad'];
                     $Conteo->id_usuario =$fila['id_usuario'];
                     $Conteo->nick = $fila['nick'];
                     $Conteo->fecha=$fila['fecha'];

                     $CONTEO[] = $Conteo;
                  }
             }
             else
             {
              print "No hay resultado ";
             }

              return $CONTEO;

      }
      catch (PDOException $ex)
      {
           print 'ERROR' . $ex -> getMessage();
      }

    }
  }
      public static function DELETE_CONTEO($conexion,$id_conteo){
        if (isset($conexion))
        {
                 try{
                   $sql = "DELETE FROM conteo WHERE id=:id_conteo";

                   $sentencia = $conexion -> prepare($sql);

                   $sentencia -> bindValue(':id_conteo',  $id_conteo, PDO::PARAM_STR);

                   $sentencia -> execute();

                 }
                 catch (PDOException $ex)
                 {
                      print 'ERROR' . $ex -> getMessage();
                 }
        }
      }
      public static function GET_COUNT_CONTEO($conexion,$id_usuario)
     	{
           		$TOTAL = null;

           		if (isset($conexion))
           		{
                       try{

                            $sql = "SELECT COUNT(*) as TOTAL from conteo WHERE id_usuario=:usuario";

                            $sentencia = $conexion -> prepare($sql);

                            $sentencia -> bindValue(':usuario',  $id_usuario, PDO::PARAM_STR);

                            $sentencia -> execute();

                            $resultado = $sentencia -> fetch();

                            $TOTAL = $resultado['TOTAL'];


                       }
                       catch (PDOException $ex)
                       {
                            print 'ERROR' . $ex -> getMessage();
                       }
           		}

                return $TOTAL;

     	}
}
?>
