 <?php

  include_once 'app/_conexion_.php';
  include_once 'app/_usuario_.php';
  include_once 'app/_control_session_.php';
  include_once 'app/_configuracion_.php';
  include_once 'app/_repo_user_.php';
  include_once 'controlador/ControladorConteo.php';

  $ControladorConteo = new ControladorConteo();

  $conteo =  $ControladorConteo->NumeroItemsContados(CONEXION::GET_CONEXION(),$_SESSION['id']);

?>

<div class="container">
  <div class="message">  </div>

  <form>

              <div class="form-group row">
                    <label  class="col-12 col-form-label">BUSCAR PRODUCTO:</label>
                          <div class="col-12">
                            <input type="text" class="form-control" id="PRODUCTOFILTRADO" placeholder="INGRESE NOMBRE O CODIGO DE PRODUCTO" required>
                          </div>
              </div>
                <div class="form-group row">
                          <div class="col-12">
                            <button type="submit" class="form-control btn btn-primary">FILTRAR PRODUCTO</button>
                          </div>
              </div><!-- cierre del button -->
 </form> <!-- cierre del formulario -->
 <div class="form-group row">
   <div class="col-12">
       <a class="form-control btn btn-info" href="/InvApp/miconteo" role="button"> Ver mi conteo (<?php echo $conteo; ?>) </a>
     </div>
 </div>
 <div id="DATA"></div>



</div> <!-- cierre del contenedor -->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
<script src="<?php echo RUTA_JS ?>jquery.js" type="text/javascript"></script>
<script>

    // Attach an event for when the user submits the form
    $('form').on('submit', function(event) {

        // Prevent the page from reloading
        event.preventDefault();

        // Set the text-output span to the value of the first input
        var $input = $(this).find('#PRODUCTOFILTRADO');
        var input = $input.val();

        $.post("app/_producto_.php", { input: input }, function(data){

          $('#DATA').html(data);


              //$('#tabla').find('#currItem').replaceWith(data);

        //$('#tabla tr').first().after(data);

});

    });

</script>
