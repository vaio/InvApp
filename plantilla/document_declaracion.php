<?php
   include_once 'app/_configuracion_.php';
?>
<!DOCTYPE html>
<html lang="es">
   <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

       <?php
         if (!isset($Titulo) || empty($Titulo))
         {
             $Titulo = 'InvApp';
         }

          echo "<title>$Titulo</title>";

       ?>

        <link rel="stylesheet" type="text/css" href="<?php echo RUTA_CSS ?>bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo RUTA_CSS ?>estilos.css">

   </head>

   <body>
