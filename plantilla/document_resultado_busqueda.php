  <div class="conteiner">
  <?php
  if(isset($empleados))
  {
  	if (count($empleados) > 0)
  	{
      foreach ($empleados as $fila)
      {
  ?>
  	     <div class="row">
  	     	   <div class="col-md-12">
  	     	   	    <div class="panel panel-default">
  	     	   	    	 <div class="panel-heading grupo_perfil">
  	     	   	    	 	  <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <a class="perfil" href="<?php echo RAUTA_HISTORIAL. '/'. $fila ->GET_ID();  ?>"><?php echo 'Ver Perfil' ?></a>
  	     	   	    	 </div>
                         <div class="panel-body">
                         	  <div class="col-md-3">

                         	  	       <?php
                                        #  echo  '<iframe class="embed-responsive-item" scrolling="NO" src="'.repo_empleado::GET_URL_FOTO($fila -> GET_FOTO()).'" ></iframe>';
                                       ?>
                            <img src=" <?php  echo RAUTA_FOTO . '/' . $fila ->GET_ID() . '.jpg'; ?> " class="img-thumbnail" alt="Cinque Terre" width="150">

                         	  </div>
                         	  <div class="col-md-9">

                         	    <div class="form-control">
                         	    <label>Nombre: </label> <?php echo $fila ->GET_NOMBRE(); ?>
                         	    </div>
                         	    <br>
                         	    <div class="form-control">
                         	    <label>Empresa: </label> <?php echo $fila ->GET_EMPRESA(); ?>
                         	    </div>
                                  <br>
                         	      <div class="form-control">
                         	    <label>Cargo: </label> <?php echo $fila ->GET_CARGO(); ?>
                         	    </div>
                                 <br>
                         	      <div class="form-control">
                         	    <label>Grupo: </label> <?php echo $fila ->GET_GRUPO(); ?>
                         	    </div>

                         	  </div>
                         </div>

  	     	   	    </div>
  	     	   </div>

  	     </div>
 <?php
  	     }
  	}
  	else
  	{
  	   ?>
  	      <div class="row">
  	     	   <div class="col-md-12">
  	     	   	    <div class="panel panel-default">
  	     	   	    	 <div class="panel-heading grupo_perfil">
  	     	   	    	 	  <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span> Resultados
  	     	   	    	 </div>
                         <div class="panel-body text-center">
                         	  <p>No hemos encontrado ningun resultado.</p>
                         	  <p>Intentalo nuevamente.</p>
                         </div>

  	     	   	    </div>
  	     	   </div>

  	     </div>
  	   <?php
  	}
   }
   else
   {
   	?>
   	   <div class="row">
  	     	   <div class="col-md-12">
  	     	   	    <div class="panel panel-default gru">
  	     	   	    	 <div class="panel-heading grupo_perfil">
  	     	   	    	 	  <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span> Resultados
  	     	   	    	 </div>
                         <div class="panel-body text-center">
                         	  <p>Los resultados de su busqueda se mostraran en esta seccion.</p>
                         </div>

  	     	   	    </div>
  	     	   </div>

  	     </div>
<?php
   }
  ?>
  </div>
